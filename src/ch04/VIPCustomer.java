package ch04;

public class VIPCustomer extends Customer{

	private int agentID;
	double salesRatio;
	
	/*
	public VIPCustomer() {
		//super(); //굳이 쓰지 않아도 컴파일러가 호출
		
		customerGrade = "VIP";    //오류 발생
		bonusRatio = 0.05;
		salesRatio = 0.1;
		
		System.out.println("VIPcustomer(int customerID,String customerName) call");
	}
	*/
	
	public VIPCustomer(int customerID, String customerName) {
		super(customerID, customerName);
		customerGrade = "VIP";    
		bonusRatio = 0.05;
		salesRatio = 0.1;
		
	}
	
	
	@Override
	public int calcPrice(int price) {
		bonusPoint += price * bonusRatio;
		price -=(int)(price*salesRatio);
		return price;
	}


	public int getAgentID() {
		return agentID;
	}

}
