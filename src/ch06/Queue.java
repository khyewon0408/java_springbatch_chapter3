package ch06;

public interface Queue {

	void enQueue (String tiltle);
	String deQueue();
	int getSize();
}
