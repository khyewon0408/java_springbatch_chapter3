package ch06;

public class BookShelfTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue bookQueue = new BookShelf();
		bookQueue.enQueue("모비딕");
		bookQueue.enQueue("노인과바다");
		bookQueue.enQueue("토지");
		
		System.out.println("권수 :: " +  bookQueue.getSize());
		System.out.println("deQueue :: " +  bookQueue.deQueue());
		System.out.println("목록 :: " +  bookQueue.getSize());
	}

}
