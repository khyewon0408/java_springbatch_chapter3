package ch06;

public class Customer implements Buy,Sell {

	@Override
	public void sell() {
		System.out.println("customer sell");
		
	}

	@Override
	public void buy() {
		System.out.println("customer buy");
		
	}

	@Override
	public void order() { // buy,sell 둘 다 갖고있는 method여서 duplicate에러 발생함 ~
		// TODO Auto-generated method stub
		// Buy.super.order();
		System.out.println("customer order");
		
	}
	
	public void hello() {
		System.out.println("hello");
	}

}
