package ch12;

public class MyLinkedList {

	private MyListNode head; //첫 노드 
	int count;
	
	public MyLinkedList() {
		
		head = null;
		count = 0;
	}
	
	public MyListNode addElement(String data) {
		MyListNode newNode;
		
		if(head ==null) {
			newNode = new MyListNode(data);
			head =newNode; // 헤드가 비었을떄 새 노드가 헤드가됨 
		}else { // 헤드가 있을때 헤드부터 마지막 노드를 찾음
			newNode = new MyListNode(data);
			MyListNode temp = head;
			while(temp.next != null) { //temp의 다음이 비어있지않으면 빈 곳을 찾을떄까지 반복
				temp =temp.next;
			}
			temp.next = newNode;
		}
		
		count ++ ;
		return newNode;
	}
	
	// LinkedList 중간에 들어가는 경우 previous node를 찾아야함 
	public MyListNode insertElement (int position, String data ) {
		
		int i ;
		MyListNode tempNode = head;
		MyListNode preNode = new MyListNode(); // 앞의 노드 
		MyListNode newNode = new MyListNode(data); // 새로 삽일할 노드 
		
		if(position < 0 || position > count) {
			return null;
			
		}
		if(position  ==  0) { // 맨 앞에 들어가는 경우 헤드부터 한 칸씩 밀림
			newNode.next = head ; 
			head = newNode;
		}else {
			for(i=0 ; i<position; i++ ) {
				System.out.println("1. preNode: " +preNode.getData());
				System.out.println("2. tempNode: " +tempNode.getData());
				
				preNode = tempNode;
				tempNode = tempNode.next;
				
				System.out.println("3. preNode: " +preNode.getData());
				System.out.println("4. tempNode: " +tempNode.getData());
			}
			System.out.println("5. newNode: " +newNode.getData());
			System.out.println("6. preNode.next: " +preNode.next.getData());
			newNode.next= preNode.next;
			System.out.println("7. preNode: " +preNode.getData());
			preNode.next=newNode;
			
		}
		count ++ ;
		return newNode;
		
	}
	
	public MyListNode removeElement(int position) {
		int i ;
		MyListNode tempNode = head;
		MyListNode preNode = null;
		
		if(position == 0) {
			head = tempNode.next;
			
		}else {
			for(i=0 ; i<position; i++ ) {
				preNode = tempNode;
				tempNode = tempNode.next;
			}
			
			preNode.next =tempNode.next;
		}
		count --;
		return tempNode;
		
	}
	
	public String getElement(int position)
	{
		int i;
		MyListNode tempNode = head;
		
		if(position >= count ){
			System.out.println("검색 위치 오류 입니다. 현재 리스트의 개수는 " + count +"개 입니다.");
			return new String("error");
		}
		
		if(position == 0){  //맨 인 경우

			return head.getData();
		}
		
		for(i=0; i<position; i++){
			tempNode = tempNode.next;
			
		}
		return tempNode.getData();
	}

	public MyListNode getNode(int position)
	{
		int i;
		MyListNode tempNode = head;
		
		if(position >= count ){
			System.out.println("검색 위치 오류 입니다. 현재 리스트의 개수는 " + count +"개 입니다.");
			return null;
		}
		
		if(position == 0){  //맨 인 경우

			return head;
		}
		
		for(i=0; i<position; i++){
			tempNode = tempNode.next;
			
		}
		return tempNode;
	}

	public void removeAll()
	{
		head = null;
		count = 0;
		
	}
	
	public int getSize()
	{
		return count;
	}
	
	public void printAll()
	{
		if(count == 0){
			System.out.println("출력할 내용이 없습니다.");
			return;
		}
		
		MyListNode temp = head;
		while(temp != null){
			System.out.print(temp.getData());
			temp = temp.next;
			if(temp!=null){
				System.out.print("->");
			}
		}
		System.out.println("");
	}
	
	public boolean isEmpty()
	{
		if(head == null) return true;
		else return false;
	}
	

	
}
