package ch12;

interface Queue{
	public void enQueue(String data);
	public String deQueue();
	public void printQueue();
}
public class MyLinkedQueue extends MyLinkedList implements Queue{
	
	MyListNode front;
	MyListNode rear;

	@Override
	public void enQueue(String data) {
		
		MyListNode newNode;
		if(isEmpty()) { // 비어있는 큐에 맨 처음으로 들어가는 경우 
			newNode = addElement(data);
			front =  newNode;
			rear = newNode;
		}else { // 이미 값이 있는 큐면 뒤에 들어감 
			newNode = addElement(data);
			rear = newNode;
		}
		
		System.out.println(newNode.getData() + " added" );
	}

	@Override
	public String deQueue() {
		if(isEmpty()) {
			return null;
		}
		
		String data = front.getData(); // 큐는 선입선출이라 front부터 제거
		
		System.out.println("front.getData():: "+front.getData()+ " front.next:: "+ front.next.getData());
		front = front.next;  // front를 제거하면 front 다음 노드가 front가 됨 
		
		if(front.next == null) { 
			rear = null;
		}
		return data;
	}

	@Override
	public void printQueue() {
		
		printAll();
		
	}

}
