package ch12;

public class MyArrayStack {

	MyArray arrayStack;
	int top;
	
	public MyArrayStack() {
	
		top = 0;
		arrayStack  =new MyArray();
	}
	
	
	public MyArrayStack(int size) {
		top =0 ;
		arrayStack = new MyArray(size);
	}
	
	public void push(int data) {
		
		if(isFull()) {
			System.out.println("error");
			return ;
		}
		arrayStack.addElement(data);
		top++;
		
	}
	
	public int pick() {
		if(isEmpty()) {
			return arrayStack.ERROR_NUM;
		}
		return arrayStack.getElement(--top);
		
	}
	
	public int pop() {
		if(isEmpty()) {
			return arrayStack.ERROR_NUM;
		}
		return arrayStack.removeElement(--top);
		
	}
	public void printAll()
	{
	arrayStack.printAll();
	}
	
	public int getSize()
	{
		return top;
	}
	
	public boolean isFull() {
		if(top ==  arrayStack.ARRAY_SIZE) {
			return true;
		}else return false;
		
	}
	
	public boolean isEmpty() {
		if(top == 0) {
			return true;
		}else {
			return false;
		}
	}
}
