package ch14;

public abstract class Material { // 직접 쓰는 경우는 없어서 추상클래스로 정의함 

	public abstract void doPrint();
}
