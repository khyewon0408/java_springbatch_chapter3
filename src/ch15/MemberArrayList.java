package ch15;

import java.util.ArrayList;
import java.util.Iterator;

public class MemberArrayList {

	private ArrayList<Member> arrayList;
	
	public MemberArrayList() {
		arrayList = new ArrayList<>();
	}

	public MemberArrayList(int size) {
		arrayList = new ArrayList<>(size);
	}
	
	public void addMember(Member member) {
		arrayList.add(member);
	}
	
	public boolean removeMember(int memberId) {
		
		/*
		for(int i = 0; i<arrayList.size(); i++) {
			
			Member member = arrayList.get(i);
			
			int memId = member.getMemberId();
			if(memId == memberId) {
				arrayList.remove(i);
				return true;
			}
			
		}
		System.out.println("memberId "+memberId +" 는 존재하지 않습니다.");
		return false;
		*/
		Iterator<Member> ir = arrayList.iterator();
		
		while(ir.hasNext()) {
			Member member = ir.next();
			if(member.getMemberId() == memberId) {
				arrayList.remove(member); //  remove는 index, object 2가지
				return true;
			}
		}
		
		return false;
	}
	
	public void showAllMember() {
		for(Member member : arrayList) {
			System.out.println("member: "+ member);
		}
		
		System.out.println();
	}
}
