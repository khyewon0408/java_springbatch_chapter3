package ch16;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

public class MemberHashSet {

	private HashSet<Member> hashSet;
	
	public MemberHashSet() {
		hashSet = new HashSet<>();
	}

	public MemberHashSet(int size) {
		hashSet = new HashSet<>(size);
	}
	
	public void addMember(Member member) {
		hashSet.add(member);
	}
	
	public boolean removeMember(int memberId) {
		
		/*
		for(int i = 0; i<arrayList.size(); i++) {
			
			Member member = arrayList.get(i);
			
			int memId = member.getMemberId();
			if(memId == memberId) {
				arrayList.remove(i);
				return true;
			}
			
		}
		System.out.println("memberId "+memberId +" 는 존재하지 않습니다.");
		return false;
		*/
		Iterator<Member> ir = hashSet.iterator();
		
		while(ir.hasNext()) {
			Member member = ir.next();
			if(member.getMemberId() == memberId) {
				hashSet.remove(member); //  remove는 index, object 2가지
				return true;
			}
		}
		
		return false;
	}
	
	public void showAllMember() {
		for(Member member : hashSet) {
			System.out.println("member: "+ member);
		}
		
		System.out.println();
	}
}
