package ch17;


import java.util.Iterator;
import java.util.TreeSet;

public class MemberTreeSet {

	private TreeSet<Member> treeSet;
	
	public MemberTreeSet() {
		treeSet =new TreeSet<Member>(new Member());
	}

	public void addMember(Member member) {
//		/System.out.println(member);
		treeSet.add(member);
	}
	
	public boolean removeMember(int memberId) {
		
		Iterator<Member> ir = treeSet.iterator();
		
		while(ir.hasNext()) {
			Member member = ir.next();
			if(member.getMemberId() == memberId) {
				treeSet.remove(member); //  remove는 index, object 2가지
				return true;
			}
		}
		System.out.println("memberId "+memberId +" 는 존재하지 않습니다.");
		return false;
	}
	
	public void showAllMember() {
		for(Member member : treeSet) {
			System.out.println("member: "+ member);
		}
		
		System.out.println();
	}
	
}
