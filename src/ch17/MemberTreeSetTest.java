package ch17;

import java.util.Comparator;
import java.util.TreeSet;


class MyCompare implements Comparator<String>{

	@Override
	public int compare(String s1, String s2) {
		return (s1.compareTo(s2)) *-1 ;
	}
}

public class MemberTreeSetTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
		TreeSet<String> set = new TreeSet<>();
		set.add("hong");
		set.add("kim");
		set.add("lee");
		
		System.out.println(set);
		*/
	/*
		MemberTreeSet memberTreeSet = new MemberTreeSet();
		
		Member memberLee = new Member(1001, "이순신");
		Member memberKim = new Member(1002, "김유신");
		Member memberKang = new Member(1003, "강감찬");
		Member memberHong = new Member(1004, "홍길동");
		Member memberTest = new Member(1004, "테스트");
		
		memberTreeSet.addMember(memberLee);
		memberTreeSet.addMember(memberKim);
		memberTreeSet.addMember(memberKang);
		memberTreeSet.addMember(memberHong);
		memberTreeSet.addMember(memberTest);
		
		memberTreeSet.showAllMember();
		
		//memberTreeSet.removeMember(memberHong.getMemberId());
	//	memberTreeSet.showAllMember();
*/
		
		TreeSet<String> set = new TreeSet<>(new MyCompare());
		set.add("hong");
		set.add("lee");
		set.add("kim");
		
		System.out.println(set);
	}

}
