package ch10;

public class StringTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String java = new String("java");
		String android = new String("android");
		
		System.out.println("메모리 확인 1: " + System.identityHashCode(java));
		java= java.concat(android);
		
		System.out.println(java);
		System.out.println("메모리 확인 2: " + System.identityHashCode(java));
		
		//위 코드처럼 쓰면 메모리에 계속 할당됨.. ㅠ 그래서 stringBuilder(단일쓰레드), stringBuffer(멀티 쓰레드)사용 
	}

}
