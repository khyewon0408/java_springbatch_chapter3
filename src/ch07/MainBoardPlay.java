package ch07;

public class MainBoardPlay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Player player = new Player() ;
		player.play(1);
		
		PlayerLevel middle = new MiddleLevel();
		player.upgradeLevel(middle);
		player.play(2);
		
		PlayerLevel high =  new HighLevel();
		player.upgradeLevel(high);
		player.play(3);
		
		/*
		PlayerLevel biginner = new BiginnerLevel();
		biginner.go(1);
		
		PlayerLevel middle = new MiddleLevel();
		middle.go(2);
		
		PlayerLevel high =  new HighLevel();
		high.go(3);
		*/
		
	}

}
