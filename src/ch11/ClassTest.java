package ch11;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ClassTest {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException,
	NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub
/*
		Class c1 = Class.forName("ch11.Person");
		Person person = (Person) c1.newInstance();
		
		person.setName("lee");
		System.out.println(person);
		
		Class c2 =person.getClass();
		Person p = (Person)c2.newInstance();
		System.out.println(c2);
		System.out.println(p);
		
		Class[] parameterType = {String.class};
		System.out.println(parameterType);
		Constructor  con = c2.getConstructor(parameterType);
		*/
//	/	
		/*
		Object[]  initargs= {"kim"};
		Person kim =(Person) con.newInstance(initargs);
		System.out.println(kim);
		*/
		
		Person person = new Person("James",50);
		System.out.println(person);
		
		Class c1 = Class.forName("ch11.Person");
		Person person1 = (Person)c1.newInstance();
		System.out.println(person1);
		
		Class[] parameterTypes = {String.class};
		Constructor cons = c1.getConstructor(parameterTypes);
		
		Object[] initargs = {"김유신"};
		Person personLee = (Person)cons.newInstance(initargs);
		System.out.println(personLee);

	}

}
