package ch09;

public class EqualTest {

	public static void main(String[] args) throws CloneNotSupportedException {
		Student student1 = new Student(1, "lee");
		Student student2 =  new Student(1,"kim");
		
		System.out.println(student1==student2);
		System.out.println(student1.equals(student2));

		
		System.out.println(student1.hashCode());
		System.out.println(student2.hashCode());
		
		System.out.println(System.identityHashCode(student1));
		
		Student cloneStd = (Student)student2.clone();
		System.out.println(cloneStd);
		
	}

	

	
}
